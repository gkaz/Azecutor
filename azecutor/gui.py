import logging as log
import sys
from pathlib import Path
from PySide2.QtWidgets import *
from PySide2.QtCore import QTimer, QObject
from typing import Union, List, Dict
import FreeCAD
from guietta import Gui as Guietta, ___, G, C, B, execute_in_main_thread

from Azecutor.azecutor.macrostep import MacroStep
import Azecutor.azecutor.parser as parser
import Azecutor.azecutor.executor as executor

Step = Union[str, MacroStep]


def show_message(text: str, icon=None):
    msg = QMessageBox()
    if icon is not None:
        msg.setIcon(icon)
    msg.setText(text)
    msg.setWindowTitle("AZecutor")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


def show_open_file_dialog(default_path: Path):
    return QFileDialog.getOpenFileName(None, 'Open file', str(default_path))[0]


class AzecutorGUI:
    def __init__(self):
        self.gui = Guietta(
            [ "Macro options:",                         ___                      ],
            [ (C("Ignore GUI commands"), "ignore_gui"), ___,                     ],
            [ ["Select"],                               "__path_to_macro__"      ],
            [ "Step delay (sec)",                       (QSpinBox, "step_delay") ],
            [ (G(""), "controls"),                      ___                      ]
        )

        self.application_title = "Azecutor"

        self.gui.controls = self.gui_controls = Guietta(
            [ (C("Auto"), "auto_mode"), ___,              ___,              "step_label"        ],
            [ (B("<<"), "to_start"),    (B("<"), "prev"), (B(">"), "next"), (B(">>"), "to_end") ]
        )

        self.gui.step_delay.setMaximum(100)

        self.steps: List[Step] = []
        self.step_contexts: List[Dict[str, object]] = []
        self.step_index = 0
        self.file_name = None
        self.revalidate()

    def revalidate(self):
        self.gui_controls.step_label = f"Step: {self.step_index}/{len(self.steps)}"
        if self.file_name != None:
            auto_mode = self.gui_controls.auto_mode.isChecked()
            self.gui_controls.prev.setEnabled(not auto_mode and self.step_index > 0)
            self.gui_controls.next.setEnabled(not auto_mode and self.step_index < len(self.steps))
            self.gui_controls.to_start.setEnabled(self.gui_controls.prev.isEnabled())
            self.gui_controls.to_end.setEnabled(self.gui_controls.next.isEnabled())
            self.gui_controls.auto_mode.setEnabled(True)
            file_name = Path(self.file_name).name
            self.gui.window().setWindowTitle(
                f"{self.application_title}: {file_name}")
        else:
            self.gui_controls.prev.setEnabled(False)
            self.gui_controls.next.setEnabled(False)
            self.gui_controls.to_start.setEnabled(False)
            self.gui_controls.to_end.setEnabled(False)
            self.gui_controls.auto_mode.setEnabled(False)
            self.gui.window().setWindowTitle(self.application_title)

    def auto_mode_changed(self, checked):
        self.revalidate()
        if checked:
            timer = QTimer(self.gui.window())

            def do_step():
                if self.step_index < len(self.steps) and self.gui_controls.auto_mode.isChecked():
                    self.controls_do_step()
                else:
                    timer.stop()

            timer.timeout.connect(do_step)
            timer.start(self.gui.widgets["step_delay"].value() * 1000)

    def window_select(self):
        param_group = FreeCAD.ParamGet("User parameter:BaseApp/Preferences/Macro")
        macro_path  = Path(param_group.GetString("MacroPath"))

        self.file_name   = show_open_file_dialog(macro_path)

        if not Path(self.file_name).is_file():
            return

        self.gui.widgets["path_to_macro"].setText(self.file_name)
        log.info(f"Processing {self.file_name}")

        with open(self.file_name) as f:
            in_lines = f.readlines()

            imports, self.steps = parser.parse_file(
                in_lines,
                self.file_name,
                ignore_gui_commands=self.gui.ignore_gui.isChecked()
            )

            imports.append("import FreeCAD as App")
            imports.append("import FreeCADGui")
            imports.append("import FreeCADGui as Gui")
            imports.append("import Show")
            imports.append("import Part")
            import_ctx = {}
            for i in imports:
                exec(executor.wrap_import_safe(i), import_ctx)
            self.step_contexts.append(import_ctx)
            self.step_index = 0
            self.gui_controls.auto_mode.setChecked(False)
            self.revalidate()

    def controls_undo_step(self):
        if self.step_index > 0:
            self.step_contexts.pop()
            FreeCAD.ActiveDocument.undo()
            self.step_index -= 1
            self.revalidate()

    def controls_do_step(self):
        if self.step_index < len(self.steps):
            log.info(f"---------- Step: {self.step_index} ----------")
            try:
                self.step_contexts.append(executor.execute_step(
                    self.steps[self.step_index],
                    self.step_contexts[-1]
                ))
            except Exception as e:
                log.error(e)
                log.error(f"Step {self.step_index} is not executed")
                show_message(f"Step {self.step_index} is not executed:\n\n{e}")
            else:
                self.step_index += 1
                self.revalidate()

    def controls_clear(self):
        for _ in range(self.step_index):
            self.controls_undo_step()

    def controls_fast_forward(self):
        for _ in range(len(self.steps) - self.step_index):
            self.controls_do_step()

    def run(self):
        self.gui.Select.clicked.connect(lambda: self.window_select())
        self.gui_controls.auto_mode.clicked.connect(lambda changed: self.auto_mode_changed(changed))
        self.gui_controls.prev.clicked.connect(lambda: self.controls_undo_step())
        self.gui_controls.next.clicked.connect(lambda: self.controls_do_step())
        self.gui_controls.to_start.clicked.connect(lambda: self.controls_clear())
        self.gui_controls.to_end.clicked.connect(lambda: self.controls_fast_forward())
        self.gui.run()
