import logging as log
from pathlib import Path
import subprocess
from distutils import sysconfig


def configure_logging():
    log.basicConfig(
        level=log.DEBUG,
        format='%(message)s'
    )

    
def load_python_path():
    '''Try to load Python path.  Return a valid Python path, or False.'''
    python_paths = [
        Path(sysconfig.PREFIX) / 'bin' / 'python3',
        Path(sysconfig.PREFIX) / 'bin' / 'python',
        Path(sysconfig.PREFIX) / 'python3',
        Path(sysconfig.PREFIX) / 'python',
        Path(sysconfig.PREFIX) / 'bin' / 'python3.exe',
        Path(sysconfig.PREFIX) / 'bin' / 'python.exe',
        Path(sysconfig.PREFIX) / 'python3.exe',
        Path(sysconfig.PREFIX) / 'python.exe'
    ]

    for pp in python_paths:
        log.info(f"Trying Python from {pp}")
        if pp.exists():
            return pp

    return False


